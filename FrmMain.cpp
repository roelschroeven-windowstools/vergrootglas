//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <memory>

#include "FrmMain.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormMagnifier *FormMagnifier;
//---------------------------------------------------------------------------

const double Magnifications[] = { 1.0, 1.5, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 15.0, 20.0, 50.0 };
//---------------------------------------------------------------------------

__fastcall TFormMagnifier::TFormMagnifier(TComponent* Owner)
  : TForm(Owner)
  , m_MagnificationIndex(4)
{
  InitComboBoxMagnifications();
  ComboBoxMagnification->ItemIndex = m_MagnificationIndex;
}
//---------------------------------------------------------------------------

double TFormMagnifier::Magnification()
{
  return Magnifications[m_MagnificationIndex];
}
//---------------------------------------------------------------------------

void TFormMagnifier::InitComboBoxMagnifications()
{
  ComboBoxMagnification->Items->BeginUpdate();
  String s;
  for (unsigned i = 0; i < ARRAYSIZE(Magnifications); ++i)
    {
    s.printf(L"%.2f", Magnifications[i]);
    ComboBoxMagnification->Items->Add(s);
    }
  ComboBoxMagnification->Items->EndUpdate();
}
//---------------------------------------------------------------------------

void __fastcall TFormMagnifier::PaintBoxLensPaint(TObject *Sender)
{
  DrawLens();
}
//---------------------------------------------------------------------------

void TFormMagnifier::DrawLens()
{
  TPoint MousePos = Mouse->CursorPos;
  int DstWidth = ClientWidth;
  int DstHeight = ClientHeight;
  int SrcWidth = DstWidth / Magnification();
  int SrcHeight = DstHeight / Magnification();
  int HalfSrcWidth = SrcWidth / 2;
  int HalfSrcHeight = SrcHeight / 2;

  HDC hDcScreen = CreateDC("DISPLAY", NULL, NULL, NULL);

  // Use a buffer bitmap to avoid flickering
  std::unique_ptr<Graphics::TBitmap> Bmp(new Graphics::TBitmap);
  Bmp->PixelFormat = pf32bit;
  Bmp->Width = SrcWidth;
  Bmp->Height = SrcHeight;

  BitBlt(
    Bmp->Canvas->Handle,
    0, 0,
    SrcWidth, SrcHeight,
    hDcScreen,
    MousePos.X - HalfSrcWidth, MousePos.Y - HalfSrcHeight,
    SRCCOPY
    );

  ReleaseDC(NULL, hDcScreen);

  Canvas->StretchDraw(TRect(0, 0, DstWidth, DstHeight), Bmp.get());
}
//---------------------------------------------------------------------------

void __fastcall TFormMagnifier::TimerDrawLensTimer(TObject *Sender)
{
  DrawLens();
}
//---------------------------------------------------------------------------

void __fastcall TFormMagnifier::ComboBoxMagnificationChange(TObject *Sender)
{
  m_MagnificationIndex = ComboBoxMagnification->ItemIndex;
  DrawLens();
}
//---------------------------------------------------------------------------

