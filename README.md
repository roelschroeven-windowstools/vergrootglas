Simpel vergrootglas
===================


De vergrootglas-app van Windows 10 start veel te traag op. Mijn programmaatje is minder uitgebreid, heeft minder mogelijkheden, maar het doet wat ik nodig heb, en voor mijn use-case is de user interface ook beter.