object FormMagnifier: TFormMagnifier
  Left = 0
  Top = 0
  Caption = 'Vergrootglas'
  ClientHeight = 635
  ClientWidth = 763
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ComboBoxMagnification: TComboBox
    Left = 8
    Top = 8
    Width = 57
    Height = 21
    Style = csDropDownList
    DropDownCount = 20
    TabOrder = 0
    OnChange = ComboBoxMagnificationChange
  end
  object TimerDrawLens: TTimer
    Interval = 100
    OnTimer = TimerDrawLensTimer
    Left = 48
    Top = 89
  end
end
