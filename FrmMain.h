//---------------------------------------------------------------------------

#ifndef FrmMainH
#define FrmMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Buttons.hpp>
//---------------------------------------------------------------------------
class TFormMagnifier : public TForm
{
__published:	// IDE-managed Components
  TTimer *TimerDrawLens;
  TComboBox *ComboBoxMagnification;
  void __fastcall PaintBoxLensPaint(TObject *Sender);
  void __fastcall TimerDrawLensTimer(TObject *Sender);
  void __fastcall ComboBoxMagnificationChange(TObject *Sender);

private:	// User declarations
  int m_MagnificationIndex;

  void InitComboBoxMagnifications();
  void DrawLens();

public:		// User declarations
  __fastcall TFormMagnifier(TComponent* Owner);

  double Magnification();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormMagnifier *FormMagnifier;
//---------------------------------------------------------------------------
#endif
